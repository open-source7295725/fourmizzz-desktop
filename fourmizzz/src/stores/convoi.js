import { analyse_nombre } from '@/utils/utils'
import { defineStore } from 'pinia'

export const useConvoiStore = defineStore('convoi', {
  state: () => ({
    headers: ['Livraison'],
    content: []
  }),
  getters: {
    message: (state) => state.content,
    header: (state) => state.headers
  },
  actions: {
    async list() {
      this.content = await window.api.listConvoi()
    },
    async sendConvoi(pomme, bois, destinataire) {

      await window.api.sendConvoi({
        pomme: analyse_nombre(pomme),
        bois: analyse_nombre(bois),
        destinataire: destinataire,
      })

      await this.list()
    },
  }
})
