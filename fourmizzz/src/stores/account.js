import AccountService from '@/services/account.service'
import { defineStore } from 'pinia'

export const useAccountStore = defineStore('account', {
  state: () => ({
    items: [],
    item: {}
  }),
  getters: {
    users: (state) => state.items,
    user: (state) => state.item
  },
  actions: {
    async addUser(pseudo, password, serveur) {
      let accounts = await AccountService.add({
        pseudo: pseudo,
        password: password,
        serveur: serveur,
        derniere_connexion: null,
      })

      this.items = accounts
    },
    async getAccounts() {
      this.items = await AccountService.getAll()
    },
    async deleteAccount(id) {
      this.items = await AccountService.delete(id)
    },
    async selectAccount(id) {
      this.item = await AccountService.selectAccount(id)
    }
  }
})
