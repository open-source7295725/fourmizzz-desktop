import { defineStore } from 'pinia'

export const useMessagerieStore = defineStore('messagerie', {
  state: () => ({
    headers: ['emetteur', 'titre', 'date'],
    content: [
      ['Fourmizzz', 'Attaque annulée contre Casterio : adversaire trop faible', '00h16'],
    ]
  }),
  getters: {
    message: (state) => state.content,
    header: (state) => state.headers
  },
  actions: {
    async list() {
      this.content = await window.api.listMessage()
    }
  }
})
