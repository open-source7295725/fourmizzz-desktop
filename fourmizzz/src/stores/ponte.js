import { defineStore } from 'pinia'

export const usePonteStore = defineStore('ponte', {
  state: () => ({
    headers: ['Unités', 'Garnison', 'Temps restant', 'Temps restant total'],
    content: [
      ["908 334 206 Jeunes Soldates Naines", "Loge Impériale", "240J 6H 12m 24s", "240J 6H 12m 24s"],
      ["908 334 206 Jeunes Soldates Naines", "Loge Impériale", "240J 6H 12m 24s", "240J 6H 12m 24s"],
      ["908 334 206 Jeunes Soldates Naines", "Loge Impériale", "240J 6H 12m 24s", "240J 6H 12m 24s"],
    ]
  }),
  getters: {
    message: (state) => state.content,
    header: (state) => state.headers
  },
  actions: {
    async list() {
      this.content = await window.api.listPonte()
    }
  }
})
