import { defineStore } from 'pinia'

export const useConstructionStore = defineStore('construction', {
  state: () => ({
    headers: ['Constructions', 'Temps restant'],
    content: [],
    items: [],
  }),
  getters: {
    message: (state) => state.content,
    header: (state) => state.headers,
    list: (state) => state.items,
  },
  actions: {
    async actual() {
      this.content = await window.api.construction()
    },
    async listeConstruction() {
      this.items = await window.api.listeConstruction()
    },
    async lancementConstruction(construction) {
      await window.api.lancementConstruction(construction)
      await this.actual()
      await this.listeConstruction()
    }
  }
})
