import { defineStore } from 'pinia'

export const useChasseStore = defineStore('chasse', {
  state: () => ({
    headers: ['Chasses en cours'],
    content: []
  }),
  getters: {
    message: (state) => state.content,
    header: (state) => state.headers
  },
  actions: {
    async list() {
      this.content = await window.api.listChasse()
    }
  }
})
