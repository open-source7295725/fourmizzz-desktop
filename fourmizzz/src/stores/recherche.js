import { defineStore } from 'pinia'

export const useRechercheStore = defineStore('recherche', {
  state: () => ({
    headers: ['Recherches', 'Temps restant'],
    content: [],
    items: [],
  }),
  getters: {
    message: (state) => state.content,
    header: (state) => state.headers,
    list: (state) => state.items
  },
  actions: {
    async actual() {
      this.content = await window.api.recherche()
    },
    async listeRecherche() {
      this.items = await window.api.listeRecherche()
    },
    async lancementRecherche(recherche) {
      await window.api.lancementRecherche(recherche)
      await this.listeRecherche()
      await this.actual()
    },
  }
})
