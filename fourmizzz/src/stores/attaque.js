import { defineStore } from 'pinia'

export const useAttaqueStore = defineStore('attaque', {
  state: () => ({
    headers: ['Cible', 'Temps restant'],
    content: [
      ['Moi', '00h16'],
    ]
  }),
  getters: {
    message: (state) => state.content,
    header: (state) => state.headers
  },
  actions: {
    async list() {
      this.content = await window.api.listAttaque()
    }
  }
})
