import { defineStore } from 'pinia'

export const useDashboardStore = defineStore('dashboard', {
  state: () => ({
    items: [],
    item: null
  }),
  getters: {
    users: (state) => state.items,
    user: (state) => state.item
  },
  actions: {}
})
