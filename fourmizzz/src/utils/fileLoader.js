// Function/fileLoader.js
const { glob } = require('glob');
const path = require('path');
const fs = require('node:fs');

async function deleteChachedFile(file) {
  const filePath = path.resolve(file);

  if (require.cache[filePath]) {
    delete require.cache[filePath];
  }
}

async function loadFiles(dirName) {
  try {
    const files = await glob(
      path.join(process.cwd(), dirName, "**/*.js").replace(/\\/g, "/")
    );

    const jsFiles = files.filter(file => path.extname(file) === ".js");

    await Promise.all(jsFiles.map(deleteChachedFile));
    return jsFiles;
  } catch (error) {
    console.log(`Erreur lors du chargement dans le dossier ${dirName} : ${error}`);
  }
}

async function checkFile(fileName) {
  try {
    return fs.existsSync(`./Data/`+fileName)
  } catch (err) {
    console.log(err)
    return false
  }
}

async function readFile(fileName) {
  try {
    if (!fs.existsSync(`./Data/${fileName}.txt`)) {
      return null
    }
    const data = fs.readFileSync(`./Data/${fileName}.txt`, 'utf8');
    return data
  } catch (err) {
    console.error(err);
    return null
  }
}

async function writeFile(fileName, content, overWrite = false) {
  
  try {
    if(overWrite === true) {
      fs.writeFile(`./Data/${fileName}.txt`, `${content}`, (err) => {
        if (err) throw err;
        
        console.log('The "data to append" was appended to file!');
        return "File create successfull"
      });
    } else {
      fs.appendFile(`./Data/${fileName}.txt`, `${content}\n`, (err) => {
        if (err) throw err;
        
        console.log('The "data to append" was appended to file!');
        return "File create successfull"
      });
    }
  } catch (err) {
    console.error(err)
    return false
  }
}

const createDirectory = async (dirName) => {
  try {
    if (!fs.existsSync('./Data/'+dirName)) {
      fs.mkdirSync('./Data/'+dirName);
    }
    return true
  } catch (err) {
    console.error(err);
    return null
  }
}

const getDirectories = () => {
  // Récupère la liste des fichiers dans le répertoire
  const directoryPath = './Data/'

  const files = fs.readdirSync(directoryPath);

  // Filtrer les fichiers pour garder seulement les dossiers
  const directories = files.filter(file => {
    const filePath = path.join(directoryPath, file);
    return fs.statSync(filePath).isDirectory();
  });

  return directories;
}

const getAccountData = async (id) => {
  return await readFile(id+'/account')
}

module.exports = {
  loadFiles,
  checkFile,
  writeFile,
  readFile,
  createDirectory,
  getAccountData,
  getDirectories,
};
