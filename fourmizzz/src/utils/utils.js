const analyse_nombre = (nombre) => {
  nombre = nombre.replace(",", ".");
  var RegExpToutSaufChiffre = new RegExp("[^0-9,/.]", "g");

  var suffixe = new RegExp("[kmgt]", "gi");
  if (nombre.match(suffixe)) {
    //début de mot - des chiffres - point facultatif - des chiffres facultatifs - suffixe - fin de mot
    var kilo = new RegExp("\\b([0-9]+)([.]*)([0-9]*)k\\b", "gi");
    var mega = new RegExp("\\b([0-9]+)([.]*)([0-9]*)m\\b", "gi");
    var giga = new RegExp("\\b([0-9]+)([.]*)([0-9]*)g\\b", "gi");
    var tera = new RegExp("\\b([0-9]+)([.]*)([0-9]*)t\\b", "gi");
    if (nombre.match(kilo)) {
      nombre = nombre.replace(RegExpToutSaufChiffre, "");
      return 1000 * nombre;
    }
    if (nombre.match(mega)) {
      nombre = nombre.replace(RegExpToutSaufChiffre, "");
      return 1000000 * nombre;
    }
    if (nombre.match(giga)) {
      nombre = nombre.replace(RegExpToutSaufChiffre, "");
      return 1000000000 * nombre;
    }
    if (nombre.match(tera)) {
      nombre = nombre.replace(RegExpToutSaufChiffre, "");
      return 1000000000000 * nombre;
    }
  }
  nombre = nombre.replace(RegExpToutSaufChiffre, "");
  return parseFloat(nombre);
};

export {
  analyse_nombre,
}