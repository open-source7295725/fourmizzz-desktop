import './assets/main.css'

import { createSSRApp } from 'vue'
import { createPinia } from 'pinia'

import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"

import App from './App.vue'
import router from './router'

const app = createSSRApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')
