import { v4 as uuidv4 } from 'uuid'

export default class AccountService {
  constructor() {}

  static getAll() {
    return window.api.getUserData('accounts')
  }

  static async add(user) {
    user.id = uuidv4()
    let accounts = await window.api.getUserData('accounts')

    if(accounts !== undefined && accounts !== null) {
      accounts.push(user)
    }
    else {
      accounts = [user]
    }

    window.api.setUserData('accounts', accounts)

    window.api.notify("vous venez d'éffectuer l'ajout d'un nouveau compte")

    return accounts
  }

  static async delete(user) {
    let accounts = await window.api.getUserData('accounts')

    accounts = accounts.filter((account) => account.id !== user)

    window.api.setUserData('accounts', accounts)

    window.api.notify("vous venez de supprimer l'un de vos comptes")

    return accounts
  }

  static async selectAccount(user) {
    let accounts = await this.getAll()

    let account = accounts.find((account) => account.id === user)

    if(!account) {
      return false
    }

    window.api.setUserData('current', account)

    window.api.notify('Un compte a été selectionné')

    await window.api.setServeur(account.serveur)
    await window.api.authenticate(account)

    return account
  }
}
