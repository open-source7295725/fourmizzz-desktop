const {
  app,
  BrowserWindow,
  ipcMain,
  Notification,
  globalShortcut,
  Menu,
  Tray,
  nativeImage,
} = require("electron");
// include the Node.js 'path' module at the top of your file
const path = require("node:path");

const Storage = require("./storage");
const CheerioService = require("./cheerio.util");

const storage = new Storage();
const cheerioService = new CheerioService();

let win = null

const createWindow = () => {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, "preload.js"),
      devTools: process.env?.NODE_ENV === "production" ? false : true,
      nodeIntegration: false,
      webSecurity: true,
      sandbox: true,
    },
    icon: path.join(__dirname, "/logo/ant"),
  });

  if (process.env?.NODE_ENV === "production") {
    win.loadFile(path.join(__dirname, "dist", "index.html"));
  } else {
    win.loadURL("http://127.0.0.1:5173");
  }

  win.on("close", (ev) => {
    win.hide()
    ev.preventDefault();
  });

  return win
};

Menu.setApplicationMenu(null);
app.whenReady().then(() => {
  
  let tray = new Tray(nativeImage.createFromPath(path.join(__dirname, "/logo/ant.png")));
  const menu = Menu.buildFromTemplate([
    {
      label: "Actions",
      submenu: [
        {
          label: "Ouvrir",
          click: (item, window, event) => {
            //console.log(item, event);
            win.show();
          },
        },
      ],
    },
    { type: "separator" },
    { role: "quit" }, // "role": system prepared action menu
  ]);
  tray.setToolTip("Fourmizzz Desktop");
  //tray.setTitle("Tray Example"); // macOS only
  tray.setContextMenu(menu);

  createWindow();

  app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") app.quit();
});

app.on("browser-window-focus", function () {
  globalShortcut.register("CommandOrControl+R", () => {
    console.log("CommandOrControl+R is pressed: Shortcut Disabled");
  });
  globalShortcut.register("F5", () => {
    console.log("F5 is pressed: Shortcut Disabled");
  });
});

app.on("browser-window-blur", function () {
  globalShortcut.unregister("CommandOrControl+R");
  globalShortcut.unregister("F5");
});

app.on("before-quit", ev => {
  // BrowserWindow "close" event spawn after quit operation,
  // it requires to clean up listeners for "close" event
  win.removeAllListeners("close");
  // release windows
});

ipcMain.handle("getUserData", async (event, key) => {
  return storage.get(key);
});

ipcMain.handle("setUserData", async (event, key, value) => {
  storage.set(key, value);

  return true;
});

ipcMain.handle("set-serveur", (event, serveur) => {
  cheerioService.setServeur(serveur);
});

ipcMain.handle("authenticate", async (event, user) => {
  return await cheerioService.authenticate(user);
});

ipcMain.handle("list-membre", async () => {
  return await cheerioService.listMembre();
});

ipcMain.handle("list-message", async (event, categorie) => {
  return await cheerioService.listMessage(categorie);
});

ipcMain.handle("set-message-alliance", async (event, message) => {
  return await cheerioService.setMessageAlliance(message);
});

ipcMain.handle("set-message-collective", async (event, items) => {
  return await cheerioService.setMessageCollective(items);
});

ipcMain.handle("list-armee", async () => {
  return await cheerioService.listArmee();
});

ipcMain.handle("liste-construction", async () => {
  return await cheerioService.listeConstruction();
});

ipcMain.handle("lancement-construction", async (event, construction) => {
  return await cheerioService.lancementConstruction(construction);
});

ipcMain.handle("construction", async (event) => {
  return await cheerioService.construction();
});

ipcMain.handle("liste-recherche", async () => {
  return await cheerioService.listeRecherche();
});

ipcMain.handle("lancement-recherche", async (event, recherche) => {
  return await cheerioService.lancementRecherche(recherche);
});

ipcMain.handle("recherche", async (event) => {
  return await cheerioService.recherche();
});

ipcMain.handle("send-convoi", async (event, convoi) => {
  let result = await cheerioService.sendConvoi(convoi);

  sendNotif(result);
  return true;
});

ipcMain.handle("liste-ponte", async (event) => {
  return await cheerioService.listPonte();
});

ipcMain.handle("liste-convoi", async (event) => {
  return await cheerioService.listConvoi();
});

ipcMain.handle("liste-chasse", async (event) => {
  return await cheerioService.listChasse();
});

ipcMain.handle("liste-attaque", async (event) => {
  return await cheerioService.listeAttaque();
});

ipcMain.on("notify", async (event, message) => {
  sendNotif(message);
});

const sendNotif = (message) => {
  const notification = new Notification({
    title: "Fourmizzz Desktop Notification",
    body: message,
    timeoutType: "default",
  });

  notification.show();
};
