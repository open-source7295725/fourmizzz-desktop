const { contextBridge, ipcRenderer } = require('electron');

// Exposer des méthodes sécurisées à l'environnement de rendu
contextBridge.exposeInMainWorld('api', {
  getUserData: async (key) => ipcRenderer.invoke('getUserData', key),
  setUserData: async (key, value) => ipcRenderer.invoke('setUserData', key, value),
  notify: (message) => ipcRenderer.send('notify', message),
  setServeur: (serveur) => ipcRenderer.invoke('set-serveur', serveur),
  authenticate: (user) => ipcRenderer.invoke('authenticate', user),
  listMembre: () => ipcRenderer.invoke('list-membre'),
  listMessage: (categorie) => ipcRenderer.invoke('list-message', categorie),
  setMessageAlliance: (message) => ipcRenderer.invoke('set-message-alliance', message),
  setMessageCollective: (items) => ipcRenderer.invoke('set-message-collective', items),
  listArmee: () => ipcRenderer.invoke('list-armee'),
  listeConstruction: () => ipcRenderer.invoke('liste-construction'),
  lancementConstruction: (construction) => ipcRenderer.invoke('lancement-construction', construction),
  construction: () => ipcRenderer.invoke('construction'),
  listeRecherche: () => ipcRenderer.invoke('liste-recherche'),
  lancementRecherche: (recherche) => ipcRenderer.invoke('lancement-recherche', recherche),
  recherche: () => ipcRenderer.invoke('recherche'),
  sendConvoi: (convoi) => ipcRenderer.invoke('send-convoi', convoi),
  listPonte: () => ipcRenderer.invoke('liste-ponte'),
  listConvoi: () => ipcRenderer.invoke('liste-convoi'),
  listChasse: () => ipcRenderer.invoke('liste-chasse'),
  listAttaque: () => ipcRenderer.invoke('liste-attaque'),
});

// Code existant pour afficher les versions des dépendances
window.addEventListener('DOMContentLoaded', () => {
  const replaceText = (selector, text) => {
    const element = document.getElementById(selector);
    if (element) element.innerText = text;
  };

  for (const dependency of ['chrome', 'node', 'electron']) {
    replaceText(`${dependency}-version`, process.versions[dependency]);
  }
});
