// const { api } = require("../Function/axios")
const axios = require("axios");
const cheerio = require("cheerio");

module.exports = class CheerioService {
  #serveur;
  #website;
  #cookie;

  constructor() {}

  setServeur(serveur) {
    this.#website = "http://" + serveur + ".fourmizzz.fr";
    this.#serveur = serveur + ".fourmizzz.fr";
  }

  async request(url, method = "GET", data) {
    const config = {
      method: method,
      url: this.#website + url,
      headers: {
        Connection: "keep-alive",
        Referer: this.#serveur,
        "Content-Type": "application/x-www-form-urlencoded",
        credentials: "same-origin",
        ...(this.#cookie && { Cookie: this.#cookie }),
      },
      data: data,
    };

    return await axios
      .request(config)
      .then((response) => response.data)
      .catch((error) => {
        console.log(error);
      });
  }

  setCookie(cookie) {
    this.#cookie = cookie;
  }

  async authenticate(user) {
    try {
      const website = await axios.get(this.#website);

      const begin = website.headers;

      const cookie = begin["set-cookie"];

      this.setCookie(cookie);

      let $ = cheerio.load(website.data);

      let deconnexion = $("#boutonDeconnexion").attr("href");

      if (deconnexion) {
        await this.request("/" + deconnexion);
      }

      const dataJson = {
        serveur: user.serveur,
        pseudo: user.pseudo,
        mot_passe: user.password,
        // sourvenir: "on",
        Connexion: "1",
      };

      const data = await this.request(
        "/index.php?connexion=1",
        "POST",
        dataJson
      );

      $ = cheerio.load(data);

      let title = $("a.titre_ressource").text();

      title = title.toLowerCase();

      if (title.includes(user.pseudo.toLowerCase())) {
        return cookie;
      } else {
        throw new Error("Authentification Failed");
      }
    } catch (error) {
      console.error("Error:", error);
      throw new Error("An error occurred during authentication");
    }
  }

  async setMessageAlliance(message) {
    try {
      const dataM = await this.request("/alliance.php");

      const $ = cheerio.load(dataM);

      const token = $("#t").val();

      const data = await this.request(
        `/appelAjax.php?actualiserChat=alliance&message=${message}&inputCouleur=000000&t=${token}`
      );

      if (data.message.includes(message)) {
        return "Message envoyé";
      }

      return "Probleme survenue lors de l'envoie du message";
    } catch (error) {
      console.error("Error:", error);
      return "An error occurred during authentication";
    }
  }

  async setMessageCollective(items) {
    try {
      const data = await this.request(`/alliance.php?messCollectif`, "POST", {
        xajax: "envoieMess",
        xajaxargs: [items.objet, items.message, items?.allMember],
      });

      if (data.includes("Message envoyé")) {
        return "Message envoyé";
      }

      return "Probleme survenue lors de l'envoie du message";
    } catch (error) {
      console.error("Error:", error);
      return "An error occurred during authentication";
    }
  }

  async lancementConstruction(construction) {
    try {
      const data = await this.request("/construction.php");

      const $ = cheerio.load(data);

      let link = "";

      $("#centre")
        .find("table:first tr.ligneAmelioration")
        .each(async (i, e) => {
          let nom = $(e).find(".desciption_amelioration h2").text();

          var imgTitle = $(e).find("td:last img").attr("title");

          if (
            nom === construction &&
            imgTitle &&
            imgTitle.includes("Construire : " + nom)
          ) {
            link = $(e).find("tr div.icone_construction a").attr("href");
          }
        });

      if (link === "") return "Construction non trouvée";

      await this.request("/" + link);

      return "Construction lancée";
    } catch (err) {}
  }

  async lancementRecherche(recherche) {
    try {
      const data = await this.request("/laboratoire.php");

      const $ = cheerio.load(data);

      let link = "";

      $("#centre")
        .find("table:first tr.ligneAmelioration")
        .each(async (i, e) => {
          let nom = $(e).find(".desciption_amelioration h2").text();

          var imgTitle = $(e).find("td:last img").attr("title");

          if (
            nom === recherche &&
            imgTitle &&
            imgTitle.includes("Rechercher : " + nom)
          ) {
            link = $(e).find("tr div.icone_recherche a").attr("href");
          }
        });

      if (link === "") return "Recherche non trouvée";

      await this.request("/" + link);

      return "Recherche lancée";
    } catch (err) {}
  }

  async listeRecherche() {
    try {
      const data = await this.request("/laboratoire.php");

      const $ = cheerio.load(data);

      let recherches = [];

      $("#centre")
        .find("table:first")
        .each((index, element) => {
          $(element)
            .find("tr.ligneAmelioration")
            .each((i, e) => {
              let recherche = [];
              let nom = $(e).find(".desciption_amelioration h2").text();
              let niveau = $(e).find(".niveau_amelioration").text();
              let description = $(e).find(".pas_sur_telephone").text();
              let temps = $(e).find(".cout_amelioration .temps").text();
              let ouvriere = $(e).find(".cout_amelioration .ouvriere").text();
              let nourriture = $(e).find(".cout_amelioration .nourriture").text();
              let materiaux = $(e).find(".cout_amelioration .materiaux").text();
              recherche.push(nom.trim());
              recherche.push(niveau.trim());
              recherche.push(description.trim());
              recherche.push(ouvriere.trim());
              recherche.push(nourriture.trim());
              recherche.push(materiaux.trim());
              recherche.push(temps.trim());

              var imgTitle = $(e).find("td:last img").attr("title");
              if (imgTitle && imgTitle.includes("Rechercher : " + nom)) {
                recherches.push(recherche);
              }
            });
        });

      return recherches;
    } catch (error) {
      console.error("Error:", error);
      return "An error occurred during operation";
    }
  }

  async sendConvoi(convoi) {
    try {
      const dataToken = await this.request("/commerce.php");

      let $ = cheerio.load(dataToken);

      const token = $("#t").val();

      const data = await this.request("/commerce.php", "post", {
        pseudo_convoi: convoi.destinataire,
        nbNourriture: convoi.pomme,
        nbMateriaux: convoi.bois,
        t: token,
        convoi: "Lancer+le+convoi",
      });
console.log(convoi)
      $ = cheerio.load(data);
console.log($("#message_convoi").text())
      return $("#message_convoi").text();
    } catch (err) {
      return "Erreur survenue pendant le convoi";
    }
  }

  async listPonte() {
    try {
      const data = await this.request("/Reine.php");

      const $ = cheerio.load(data);

      let pontes = [];

      $(".tableau_leger")
        .find("tbody tr:nth-child(n+2)")
        .each((index, element) => {
          let ponte = [];
          $(element)
            .find("td:nth-child(n+1):nth-child(-n+5)")
            .each((i, e) => {
              ponte.push($(e).text());
            });
          pontes.push(ponte);
        });

      return pontes;
    } catch (error) {
      console.error("Error:", error);
      return "An error occurred during operation";
    }
  }

  async listConvoi() {
    try {
      const data = await this.request("/commerce.php");

      const $ = cheerio.load(data);

      let convois = [];

      $("strong:nth-child(n+2)").each((index, element) => {
        let convoi = $(element).text();
        convois.push([convoi]);
      });

      return convois;
    } catch (error) {
      console.error("Error:", error);
      return "An error occurred during operation";
    }
  }

  async listChasse() {
    try {
      const data = await this.request("/Ressources.php");

      const $ = cheerio.load(data);

      let chasses = [];

      $("#boite_tdc span:nth-child(n+5)").each((index, element) => {
        if (!$(element).attr("id") && !$(element).attr("class")) {
          let chasse = $(element).text();
          chasses.push([chasse]);
        }
      });

      return chasses;
    } catch (error) {
      console.error("Error:", error);
      return "An error occurred during operation";
    }
  }

  async listMembre() {
    try {
      let dataJson = {
        xajax: "membre",
      };
      const data = await this.request(
        "/alliance.php?Membres",
        "POST",
        dataJson
      );

      const $ = cheerio.load(data);

      let members = [
        ["Rang", "Pseudo", "Terrain", "Technologie", "Fourmiliere"],
      ];

      $("#tabMembresAlliance")
        .find("tr:nth-child(n+1)")
        .each((index, element) => {
          let arr = [];
          $(element)
            .find("td:nth-child(n+3):nth-child(-n+9)")
            .each((i, e) => {
              let el = $(e).text().trim(); // Trim to remove leading/trailing whitespace
              if (el) {
                // Check if the element is not empty
                arr.push(el);
              }
            });
          if (arr.length > 0) {
            members.push(arr);
          }
        });

      return members;
    } catch (error) {
      console.error("Error:", error);
      return "An error occurred during authentication";
    }
  }

  async listMessage(categorie = "Reception") {
    try {
      let dataJson = {
        traitement: "afficher",
        dossier_special: categorie,
      };
      const data = await this.request("/messagerie.php", "POST", dataJson);

      const $ = cheerio.load(data);

      let table = [];

      $("#table_liste_conversations tbody")
        .find("tr:nth-child(n+3):nth-child(-n+12)")
        .each((index, element) => {
          let emetteur = $(element).find(".td_participants").text();
          let objet = $(element).find("a.intitule_message").text();
          let date = $(element).find("span").text();

          table.push([emetteur, objet, date]);
        });

      return table;
    } catch (error) {
      console.error("Error:", error);
      return [];
    }
  }

  async listArmee() {
    try {
      const data = await this.request("/Armee.php");

      const $ = cheerio.load(data);

      let armees = [];

      let title = [];
      $(".simulateur:first")
        .find("tr:nth-child(2)")
        .each((index, element) => {
          $(element)
            .find("td")
            .each((i, e) => {
              title.push($(e).text().trim());
            });
        });

      armees.push(title);

      $(".simulateur:first")
        .find("tr:nth-child(n+3)")
        .each((index, element) => {
          let arr = [];
          $(element)
            .find("td")
            .each((i, e) => {
              if ($(e).css("width") !== "20px") {
                let el = $(e).text().trim(); // Trim to remove leading/trailing whitespace
                arr.push(el);
              }
            });
          armees.push(arr);
        });

      return armees;
    } catch (error) {
      console.error("Error:", error);
      return "An error occurred during operation";
    }
  }

  async listeConstruction() {
    try {
      const data = await this.request("/construction.php");

      const $ = cheerio.load(data);

      let constructions = [];

      $("#centre")
        .find("table:first")
        .each((index, element) => {
          $(element)
            .find("tr.ligneAmelioration")
            .each((i, e) => {
              let construction = [];
              let nom = $(e).find(".desciption_amelioration h2").text();
              let niveau = $(e).find(".niveau_amelioration").text();
              let description = $(e).find(".pas_sur_telephone").text();
              let temps = $(e).find("td.temps").text();
              let resource = $(e).find("td.materiaux").text();
              construction.push(nom.trim());
              construction.push(niveau.trim());
              construction.push(description.trim());
              construction.push(resource.trim());
              construction.push(temps.trim());

              var imgTitle = $(e).find("td:last img").attr("title");
              if (imgTitle && imgTitle.includes("Construire : " + nom)) {
                constructions.push(construction);
              }
            });
        });

      return constructions;
    } catch (error) {
      console.error("Error:", error);
      return "An error occurred during operation";
    }
  }

  async construction() {
    try {
      const data = await this.request("/construction.php");

      const $ = cheerio.load(data);

      let constructions = [];

      $("strong")
        .each((index, element) => {
          let construction = $(element).text()
          construction = construction.split('terminé')[0]
          let temps = $(element).find('span').text()

          constructions.push([construction, temps])
        });

      return constructions;
    } catch (error) {
      console.error("Error:", error);
      return "An error occurred during operation";
    }
  }

  async recherche() {
    try {
      const data = await this.request("/laboratoire.php");

      const $ = cheerio.load(data);

      let recherches = [];

      $("strong")
        .each((index, element) => {
          let recherche = $(element).text()
          recherche = recherche.split('terminé')[0]
          let temps = $(element).find('span').text()

          recherches.push([recherche, temps])
        });

      return recherches;
    } catch (error) {
      console.error("Error:", error);
      return "An error occurred during operation";
    }
  }

  async listeAttaque() {
    try {
      const data = await this.request("/Armee.php");

      const $ = cheerio.load(data);

      let attaques = [];
      let cible = null
      let temps = null

      $("span")
        .each((index, element) => {

          if($(element).attr('class')==="gras") {
            if(temps !== null) {
              attaques.push([cible, temps])
              temps = null
            }
  
            if($(element).attr('id')) {
              temps = $(element).text()
            }
            else {
              cible = $(element).text()
            }
          }
        });

        if(temps !== null) {
          attaques.push([cible, temps])
          temps = null
        }

      return attaques;
    } catch (error) {
      console.error("Error:", error);
      return "An error occurred during operation";
    }
  }
};
