// storage.js
class Storage {
  constructor() {
    this.store = null;
    this.init();
  }

  async init() {
    const { default: Store } = await import('electron-store');
    this.store = new Store();
  }

  async get(key) {
    await this.ensureStoreIsReady();
    return this.store.get(key);
  }

  async set(key, value) {
    await this.ensureStoreIsReady();
    this.store.set(key, value);
  }

  async ensureStoreIsReady() {
    if (!this.store) {
      await this.init();
    }
  }
}

module.exports = Storage;
